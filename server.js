const express = require('express')
const cors = require('cors')
const download = require('image-downloader')
const axios = require('axios')
const app = express()
app.use(cors())

const musikki = {
  headers: { 
    appkey: '431d066d9df496d9a9f6806ed7d32a0e',
    appid: '27ebcfe546cb8b114bbea7ecfc0634ba' 
  }
}

// get artist general
// app.get('/musikki/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/bio`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // Get information about artist
// app.get('/musikki/bio/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/bio`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // Get artist pictures
// app.get('/musikki/images/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/images`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// get artist albums
app.get('/musikki/albums/:artist', (req, res) => {
    axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
      .then(response => {
        // res.send(response.data.results[0])
      return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/releases?q=[release-type:Album],[release-subtype:Studio]`, musikki)
        .then(response => {
          res.send(response.data.results)
        })
        .catch(error => {
          console.log(error)
        })
      })
      .catch(error => {
        console.log(error)
      })
})

// app.get('/musikki/social/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/social?q=[service-name:twitter]`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // get album info
// app.get('/musikki/:artist/:album', (req, res) => {
//   axios.get(`https://music-api.musikki.com/v1/releases?q=[artist-name:${req.params.artist}],[release-title:${req.params.album}]`, musikki)
//     .then(response => {
//       // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/releases/${response.data.results[0].mkid}`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//     })
//     .catch(error => {
//       console.log(error)
//     })
// })

// // get album reviews
// app.get('/musikki/reviews/:artist/:album', (req, res) => {
//   axios.get(`https://music-api.musikki.com/v1/releases?q=[artist-name:${req.params.artist}],[release-title:${req.params.album}]`, musikki)
//     .then(response => {
//       // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/releases/${response.data.results[0].mkid}/reviews`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//     })
//     .catch(error => {
//       console.log(error)
//     })
// })

app.listen(3000)

