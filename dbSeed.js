const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : 'sef-70-db.csl14mky3k5o.us-west-2.rds.amazonaws.com',
  user     : 'snare',
  password : 'BIR29SPV',
  database : 'snare'
});
const SpotifyWebApi = require('spotify-web-api-node');
const express = require('express')
const cors = require('cors')
const axios = require('axios')
const app = express()
app.use(cors())
connection.connect();


const spotifyApi = new SpotifyWebApi({
  clientId : '5d1de969350d41c2a2c36ff9dd9c02db',
  clientSecret : '5316051397d940298af3274e3b563772'
})

const musikki = {
  headers: { 
    appkey: '431d066d9df496d9a9f6806ed7d32a0e',
    appid: '27ebcfe546cb8b114bbea7ecfc0634ba' 
  }
}


// spotifyApi.clientCredentialsGrant()
  // .then(response => {
  //   console.log('The access token expires in ' + response.body['expires_in']);
  //   console.log('The access token is ' + response.body['access_token']);

  //   // Save the access token so that it's used in future calls
  //   spotifyApi.setAccessToken(response.body['access_token']);
  // })
  // .catch(error => {
  //   console.log('Something went wrong when retrieving an access token', error.message);
  // })
// BQBZy8Wu75aMoFdQiQyQ206J0V9Hrb1ddg9-vHJdZjTmn955EmIB75VuITKcSszXf18TlswQA6308asJ3TTH8w


const seedArtistNames = artist => {
  connection.query(`INSERT INTO artists (artist_name) VALUES ('${artist}')`, (err, result) => {
    if (err) throw err;
    console.log(`Changed ${result.changedRows} row(s)`);
    })
}

const fillArtistGenre = artist => {
  // hi aws
  spotifyApi.setAccessToken('BQBvSFSfuv2enfe_aBUql8A49Bq14-GXuZ8Vlknp6sGDk1LOec3VBbBxmcxQXOnIvI88Fh7EBh_mD7Tsi43CbA');
  spotifyApi.searchArtists(artist)
  .then(response => {
  let genre =  [response.body.artists.items[0].genres[0]]
  connection.query(`UPDATE artists SET artist_genre = "${genre.filter(x => x)}" WHERE artist_name = "${artist}"`, (err, result) => {
    if (err) throw err;
    console.log(`Changed ${result.changedRows} row(s)`);
    })
  })
  .catch(error => {
    console.error(error)
  })
}

const fillArtistLocation = artist => {
   axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${artist}]`, musikki)
      .then(response => {
        let origin = [
          response.data.results[0].location.start.city,
          response.data.results[0].location.start.state,
          response.data.results[0].location.start.country.name
        ]
        // let artist_origin = origin.filter(o => o)
        connection.query(`UPDATE artists SET artist_origin = "${origin.filter(o => o)}" WHERE artist_name = "${artist}"`, (err, result) => {
        if (err) throw err;

        console.log(`Changed ${result.changedRows} row(s)`);
        })
      })
      .catch(error => {
        console.log(error)
      })
}

const fillArtistDescription = artist => {
   axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${artist}]`, musikki)
      .then(response => {
      return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/bio`, musikki)
        .then(response => {
          const description = {
            summary: response.data.summary.replace(/"/g, '&quot;'),
            full: [ 
            response.data.full[0].text[0],
            response.data.full[0].text[1],
            response.data.full[0].text[2],
            response.data.full[1].text[0],
            response.data.full[1].text[1],
            ]
          }
          console.log(description.summary)
          connection.query(`UPDATE artists SET artist_description = "${description.summary.replace(/'/g, '&quot;')}" WHERE artist_name = "${artist}"`, (err, result) => {
            if (err) throw err;

            console.log(`Changed ${result.changedRows} row(s)`);
            })
          console.log(description.full.filter(d => d))

        })
        .catch(error => {
          console.log(error)
        })
      })
      .catch(error => {
        console.log(error)
      })
}
// _Profile.jpe
//  ../../../
//  snare/src/comp
//  ../../../images/lastfm
const fillArtistPicture = artist => {
  connection.query(`UPDATE artists SET artist_profile_picture = "../../static/lastfm/${artist}_Profile.jpeg" WHERE artist_name = "${artist}"`, (err, result) => {
  if (err) throw err;

  console.log(`Changed ${result.changedRows} row(s)`);
  })
}

// fillArtistDescription('Radiohead')
// fillArtistLocation('Slowdive')
// fillArtistDescription('Metallica')


const artistList = [
      'Radiohead',
      'Deafheaven',
      'Elliott Smith',
      'Danny Brown',
      'Burial',
      'american football',
      'big L',
      'black Flag',
      'Car Seat Headrest',
      'Cibo Matto',
      'Megadeth',
      'La Dispute',
      'Kyuss',
      'Queens of the Stone Age',
      'Minor Threat',
      'Peeping Tom'
      'The Velvet Underground',
      'Talking Heads',
      'Raekwon',
      'Quasimoto',
      'Neurosis',
      'Gorillaz',
      'Slowdive',
      'Shlohmo',
      'Metallica'
      ]

// artistList.map(artist => {
//   return seedArtistNames(artist)
// })

// artistList.map(artist => {
//   return fillArtistGenre(artist)
// })


// artistList.map(artist => {
//   return fillArtistLocation(artist)
// })

// artistList.map(artist => {
//   return fillArtistDescription(artist)
// })

// artistList.map(artist => {
//   return fillArtistPicture(artist)
// })

// let origin = {
//           twice : x => x*x
//         }

// console.log(origin.twice(5))

// fillArtistLocation('Shlohmo')


// // get artist general
// app.get('/musikki/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         res.send(response.data.results[0])
//         let origin = [
//           response.data.results[0].location.start.city,
//           response.data.results[0].location.start.state,
//           response.data.results[0].location.start.country.name
//         ]
//         // let artist_origin = origin.filter(o => o)
//         res.send(origin)
//       // return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/bio`, musikki)
//       //   .then(response => {
//       //     res.send(response.data)
//       //   })
//       //   .catch(error => {
//       //     console.log(error)
//       //   })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // Get information about artist
// app.get('/musikki/bio/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/bio`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // Get artist pictures
// app.get('/musikki/images/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/images`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // get artist albums
// app.get('/musikki/albums/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/releases?q=[release-type:Album],[release-subtype:Studio]`, musikki)
//         .then(response => {
//           res.send(response.data.results)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// app.get('/musikki/social/:artist', (req, res) => {
//     axios.get(`https://music-api.musikki.com/v1/artists?q=[artist-name:${req.params.artist}]`, musikki)
//       .then(response => {
//         // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/artists/${response.data.results[0].mkid}/social?q=[service-name:twitter]`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//       })
//       .catch(error => {
//         console.log(error)
//       })
// })

// // get album info
// app.get('/musikki/:artist/:album', (req, res) => {
//   axios.get(`https://music-api.musikki.com/v1/releases?q=[artist-name:${req.params.artist}],[release-title:${req.params.album}]`, musikki)
//     .then(response => {
//       // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/releases/${response.data.results[0].mkid}`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//     })
//     .catch(error => {
//       console.log(error)
//     })
// })

// // get album reviews
// app.get('/musikki/reviews/:artist/:album', (req, res) => {
//   axios.get(`https://music-api.musikki.com/v1/releases?q=[artist-name:${req.params.artist}],[release-title:${req.params.album}]`, musikki)
//     .then(response => {
//       // res.send(response.data.results[0])
//       return axios.get(`https://music-api.musikki.com/v1/releases/${response.data.results[0].mkid}/reviews`, musikki)
//         .then(response => {
//           res.send(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//     })
//     .catch(error => {
//       console.log(error)
//     })
// })


// app.get('/spotify', (req, res) => {
//   spotifyApi.searchArtists('radiohead')
//   .then(response => {
//     // res.send(response.body);
//     res.send(response.body.artists.items[0]);
//   })
//   .catch(error => {
//     console.error(error)
//   })
// })

app.listen(8000)


 // const origin = {
        //   city:    c => (c ? c: ''),
        //   state:   s => (s ? s: ''),
        //   country: c => (c ? c: '')
        // }
        // console.log(origin.state(response.data.results[0].location.start.city))