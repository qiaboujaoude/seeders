const express = require('express')
const cors = require('cors')
const download = require('image-downloader')
var axios = require('axios')
var app = express()
app.use(cors())

const lastfmKey = '04bcaf9ea18a47f83b06b975469dd393'

function lastFMArtistPictureDownloader(artist) {
  axios.get('http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=' + artist +
            '&api_key=' + lastfmKey + '&format=json')
        .then(response => {
          let options = {
            url: response.data.artist.image[3]["#text"].replace('300x300', '1200x1200'),
            dest: `./test/${artist}_profile_fm.jpg`
          }
          download.image(options)
            .then(({ filename, image }) => {
              console.log('Success: File saved to', filename)
            })
            .catch(error => {
              console.log(error)
            })
        })
        .catch(error => {
          console.log(error)
        })
}

app.listen(3000)

